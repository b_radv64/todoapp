Brad Wilfong
12/2/2019
Assignment 8
BitBucket Link: https://bitbucket.org/b_radv64/todoapp/src/master/

1. The exception that gets thrown when accessing the internet without permissions is SecruityException and the exception for connceting to the internet on a UI thread is NetworkOnMainException.
2. A REST API defines a set of functions which developers can perform requests and receive responses via HTTP protocol such as GET and POST. The 4 HTTP methods are GET, PUT, POST, DELETE.
3. A ThreadPoolExecutor uses a pool of threads. The benefit of using one over spawning plain threads is that it's size configuable.
4. Broadcast Receivers subscribe to the system events and broadcast them within your app.
5. The methods for AsyncTask that run in the background are doInBackground() method. The methods that run on the UI thread are onPostExecute(), onPreExecute() and onProgressUpdate().
6. It is important to run expensive tasks on the background thread to prevent slow down on the UI thread. The consequences are an unpleasant user experience.
7. The role of a Handler in Andriod is it allows communicating back with UI thread from other background thread.
8. The drawback of using AsyncTask is once the device is rotated then the app is destroyed and recreated and the AsyncTask reference is no longer valid. The drawback of using regular java threads is mulitple threads interfere with one another when they share hardware.
9. 2 ways are AsyncTask and handlers.
10. To update a notification that is already displayed you could send another notification with the same ID.
