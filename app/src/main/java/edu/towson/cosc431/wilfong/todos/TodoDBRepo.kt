package edu.towson.cosc431.wilfong.todos

import android.content.Context
import edu.towson.cosc431.wilfong.todos.interfaces.TODORepository
import edu.towson.cosc431.wilfong.todos.models.TODOList

class TodoDBRepo(ctx: Context): TODORepository {

    private val db: IDatabase

    init {
        db = TodoDatabase(ctx)
    }

    override fun getCount(): Int {
        return db.getTodos().size
    }

    override fun getTask(idx: Int): TODOList? {
        return db.getTodo(idx)
    }

    override fun getAll(): List<TODOList> {
       return db.getTodos()
    }

    override fun completed(): List<TODOList> {
        return db.getCompleted()
    }

    override fun remove(task: TODOList) {
        db.deleteTodo(task)
    }

    override fun updateTask(task: TODOList) {
        db.updateTodo(task)
    }

    override fun addTask(task: TODOList) {
        db.addTodo(task)
    }
    override fun active(): List<TODOList> {
        return db.getActive()
    }
    override fun addAllTaskdb(task: List<TODOList>){
        if(db.getTodos().isEmpty()){
            for(x in 0 until task.size){
                db.addTodo(task[x])
            }
        }

    }





}