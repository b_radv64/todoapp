package edu.towson.cosc431.wilfong.todos

import android.app.Application
import com.androidnetworking.AndroidNetworking

class TODOApplicaiton : Application() {
    override fun onCreate() {
        super.onCreate()
        AndroidNetworking.initialize(this)
    }
}