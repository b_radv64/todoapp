package edu.towson.cosc431.wilfong.todos

import edu.towson.cosc431.wilfong.todos.interfaces.TODOCache
import edu.towson.cosc431.wilfong.todos.models.TODOList

class TODORepo : TODOCache{
    private var todos: MutableList<TODOList> = mutableListOf()
    private var filter: MutableList<TODOList> = mutableListOf()

    init {
        //val seed = (1..10).map { idx -> TODOList(idx, "Todo${idx}", "Contents${idx}", idx % 2 == 0, "Date Created${idx}") }
        //todos.addAll(seed)
        //filter.addAll(seed)
    }

    override fun addTask(task: TODOList) {
        todos.add(task)
        filter.add(task)
    }

    override fun addTaskFilter(task: TODOList) {
        todos.add(task)
    }

    override fun removeEverything() {
        todos.clear()
    }

    override fun filterRemoveEverything() {
        filter.clear()
    }


    override fun updateTask(idx: Int, task: TODOList) {
        todos.set(idx, task)
        filter.set(idx, task)
    }


    override fun getCount(): Int {
        return todos.size
    }

    override fun getFilterCount(): Int {
        return filter.size
    }

    override fun getTask(idx: Int): TODOList {
        return todos.get(idx)
    }

    override fun getFilterTask(idx: Int): TODOList {
        return filter.get(idx)
    }


    override fun getAll(): List<TODOList> {
        return todos
    }

    override fun remove(task: TODOList) {
        todos.remove(task)
        filter.remove(task)
    }

    override fun replace(idx: Int, task: TODOList) {
        if(idx >= todos.size) throw Exception("Outside of bounds")
        todos[idx] = task
    }

    override fun refresh(tasks: List<TODOList>) {
        this.todos.clear()
        this.todos.addAll(tasks)
    }
    override fun addAllTask(task:List<TODOList>) {
        todos.addAll(task)
    }
}