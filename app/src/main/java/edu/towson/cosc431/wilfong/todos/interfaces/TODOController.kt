package edu.towson.cosc431.wilfong.todos.interfaces

import android.graphics.Bitmap
import edu.towson.cosc431.wilfong.todos.models.TODOList

interface TODOController {
    fun deleteTask(idx: Int)
    fun completeCheck(idx: Int)
    fun editTask(idx: Int)
    fun launchNewScreen()
    fun getIcon(iconUrl: String, callback: (Bitmap) -> Unit)
    val taskCache: TODOCache
}