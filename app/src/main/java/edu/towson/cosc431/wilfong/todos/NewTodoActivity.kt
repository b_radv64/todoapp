package edu.towson.cosc431.wilfong.todos

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import edu.towson.cosc431.wilfong.todos.models.TODOList
import kotlinx.android.synthetic.main.new_todo_activity.*
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*



class NewTodoActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.new_todo_activity)
        checkCode()

    }

    private fun checkCode() {
        val json = intent?.getStringExtra("Task")
        val ss = Gson().fromJson(json, TODOList::class.java)
        val id = intent?.getStringExtra("ID")
        if (id == "1") {
            titleText.setText(ss.title)
            contentsText.setText(ss.contents)
            checkComplete.isChecked = ss.isCompleted
        }
        btn_Save.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        val json = intent?.getStringExtra("Task")
        val checkID = Gson().fromJson(json, TODOList::class.java)
        val idx = intent?.getStringExtra("ID")
        var id = -1
        if(idx == "1"){
            id = checkID.id
        }
        val index = intent?.getStringExtra("TaskNumber")
        when (view?.id) {
            R.id.btn_Save -> {
                val intent = Intent()
                val title = titleText.editableText.toString()
                val contents = contentsText.editableText.toString()
                val isCompleted = checkComplete.isChecked
                //val image = "Hello World"
                val date = Date()
                val formatter = SimpleDateFormat("MMM dd yyyy")
                val dateCreated: String = formatter.format(date)
                try {

                    val todo = TODOList(id, title, contents, isCompleted, dateCreated)
                    val json = Gson().toJson(todo)
                    Toast.makeText(this, json.toString(), Toast.LENGTH_SHORT).show()
                    intent.putExtra(Todo_Key, json)
                    intent.putExtra("Index", index)
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                } catch (ex: Exception) {
                    Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
                }

            }
        }

    }
    companion object {
        val Todo_Key = "Todo"
    }
}

