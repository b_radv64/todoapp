package edu.towson.cosc431.wilfong.todos

import android.app.Activity
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import edu.towson.cosc431.wilfong.todos.interfaces.TODOCache
import edu.towson.cosc431.wilfong.todos.interfaces.TODORepository
import edu.towson.cosc431.wilfong.todos.interfaces.TODOController
import edu.towson.cosc431.wilfong.todos.models.TODOList
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.io.FileOutputStream
import java.lang.Exception
import java.net.URL
import java.util.concurrent.Executors
import java.util.concurrent.ThreadPoolExecutor
import javax.net.ssl.HttpsURLConnection

class MainActivity : AppCompatActivity(), TODOController, SortingFragment.OnTaskChange{
    override fun onTaskChange(filter: SortingFragment.OnTaskChange.Rating) {
        try {
            var x = taskCache.getFilterCount()
            taskCache.removeEverything()
            for(z in 0 until x){
                when(filter.toString()){
                    "All"->{
                        taskCache.refresh(TodoDb.getAll())
                    }
                    "Active"->{
                        taskCache.refresh(TodoDb.active())
                    }
                    "Completed"-> {
                        taskCache.refresh(TodoDb.completed())
                    }
                }
            }
            recyclerview.adapter?.notifyDataSetChanged()

        } catch (ex: Exception) {
            Toast.makeText(this, ex.toString(), Toast.LENGTH_SHORT).show()
        }

    }


    override fun deleteTask(idx: Int) {
        val current = taskCache.getTask(idx)
        if (current != null) {
            TodoDb.remove(current)
            taskCache.refresh(TodoDb.getAll())
        }

    }

    override fun editTask(idx: Int){
        val current = taskCache.getTask(idx)
        val json = Gson().toJson(current)
        val intent = Intent(this,NewTodoActivity::class.java)
        intent.putExtra("Task", json)
        intent.putExtra("ID", "1")
        intent.putExtra("TaskNumber", idx.toString())
        startActivityForResult(intent, ADD_TODO_CHANGE_CODE)
    }
    override fun completeCheck(idx: Int) {
        val lists = taskCache.getTask(idx)
        val newTask = lists.copy(isCompleted = !lists.isCompleted)
        TodoDb.updateTask(newTask)
        taskCache.refresh(TodoDb.getAll())
    }
    lateinit var receiver: MyReceiver
    lateinit var handler: Handler
    lateinit var threadPool: ThreadPoolExecutor
    lateinit var TodoDb: TODORepository
    override lateinit var taskCache: TODOCache


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        createNotificationChannel()
        receiver = MyReceiver()
        handler = Handler()


        try {
            taskCache = TODORepo()
            TodoDb = TodoDBRepo(this)
            taskCache.refresh(TodoDb.getAll())

        } catch (ex: Exception) {
            Toast.makeText(this, ex.toString(), Toast.LENGTH_SHORT).show()
        }

        val adapter = TODOAdapter(this)

        recyclerview.adapter = adapter

        recyclerview.layoutManager = LinearLayoutManager(this)

        val frag = supportFragmentManager
            .findFragmentById(R.id.fragment_layout)

        if(frag != null){
            if(frag is SortingFragment) {
                frag.setOnTaskChange(this)
            }
        }
        try{
            TODONetworking()
                .fetchTask { TODO ->
                    Log.d("check1", TODO.toString())
                    taskCache.removeEverything()
                    TodoDb.addAllTaskdb(TODO)
                    taskCache.refresh(TodoDb.getAll())
                    recyclerview.adapter?.notifyDataSetChanged()
                }
        }catch (ex: Exception) {
            Toast.makeText(this, ex.toString(), Toast.LENGTH_SHORT).show()
        }
        main_button.setOnClickListener { launchNewScreen() }
    }

    override fun onResume()  {
        super.onResume()
        recyclerview.adapter?.notifyDataSetChanged()
        registerReceiver(receiver, IntentFilter(Intent.ACTION_AIRPLANE_MODE_CHANGED))


    }
    override fun getIcon(iconUrl: String, callback: (Bitmap) -> Unit) {
        threadPool = Executors.newFixedThreadPool(2) as ThreadPoolExecutor
        val cachedBitmap = checkCache(iconUrl)
        if(cachedBitmap == null) {
            threadPool.execute {
                Thread.sleep(10000)
                TODONetworking()
                    .fetchIcon(iconUrl, callback)
                    val url_url = URL(iconUrl)
                    val connection = url_url.openConnection() as HttpsURLConnection?
                    if (connection == null) {
                         //callback(null)
                    } else {
                        connection.doInput = true
                        connection.requestMethod = "GET"
                        connection.connect()
                        val bitmap = BitmapFactory.decodeStream(connection.inputStream)
                        cacheFile(iconUrl, bitmap)
                        handler.post {
                            callback(bitmap)
                        }
                    }
                val id = THREAD_ID++
                val copy = id
                showNotification(copy, 0)
                }
            } else {
                callback(cachedBitmap)

            }
    }
    private fun cacheFile(url: String, bitmap: Bitmap?) {
        if(bitmap != null) {
            getLast2PathSegmentsAsString(url)?.let { filename ->
                val cacheFile = File(cacheDir, filename)
                val fos = FileOutputStream(cacheFile)
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos)
                fos.close()
            }
        }
    }

    private fun getLast2PathSegmentsAsString(url: String) : String? {
        val segments = Uri.parse(url)?.pathSegments
        val last2 = segments?.subList(segments.size - 2, segments.size)
        return last2?.reduce { acc, s -> acc + s }
    }

    private fun checkCache(url: String): Bitmap? {
        getLast2PathSegmentsAsString(url)?.let { filename ->
            val cacheFile = File(cacheDir, filename)
            if(cacheFile.exists()) {
                return BitmapFactory.decodeStream(cacheFile.inputStream())
            } else {
                return null
            }
        }
        return null
    }

    override fun launchNewScreen() {
        val intent = Intent(this, NewTodoActivity::class.java)
        startActivityForResult(intent, ADD_TODO_REQUEST_CODE)

    }
    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "SERVICE_DEMO_CHANNEL"
            val descriptionText = "Notification channel for ServiceDemo"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(NOTIF_ID, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }
    private fun showNotification(id: Int, progress: Int) {
        val intent = Intent(this, MainActivity::class.java)
            .putExtra("EXTRA", "This is a meesage from a pending intent")
            .putExtra(NOTIF_ID, id)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        val builder = NotificationCompat.Builder(this, NOTIF_ID)
        builder.setContentTitle("Image downloaded")
            .setContentText("Click this notification to launch the Activity")
            .setSmallIcon(android.R.drawable.btn_dialog)
            .setVibrate(longArrayOf(1000L))
            .setContentIntent(pendingIntent)
            .setProgress(10, progress, false)
            .setTicker("Hello World")
            .setAutoCancel(true)
        val notification = builder.build()
        NotificationManagerCompat.from(this).notify(id, notification)
    }
    
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(resultCode) {
                Activity.RESULT_OK -> {
                    when(requestCode){
                        ADD_TODO_REQUEST_CODE -> {
                            val json = data?.getStringExtra(NewTodoActivity.Todo_Key)
                            if(json != null){
                                val todoList: TODOList = Gson().fromJson(json, TODOList::class.java)
                                TodoDb.addTask(todoList)
                                taskCache.refresh(TodoDb.getAll())
                            }
                        }
                        ADD_TODO_CHANGE_CODE -> {
                            val index = data?.getStringExtra("Index")
                            val json = data?.getStringExtra(NewTodoActivity.Todo_Key)
                            if(json != null){
                                val todoList: TODOList = Gson().fromJson(json, TODOList::class.java)
                                if (index != null) {
                                    TodoDb.updateTask(todoList)
                                    taskCache.refresh(TodoDb.getAll())
                                }
                            }
                        }
                    }

                }
                Activity.RESULT_CANCELED -> {
                    Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
                }
                else -> Toast.makeText(this, requestCode.toString(), Toast.LENGTH_SHORT).show()
        }

    }
    companion object {
        val ADD_TODO_REQUEST_CODE = 1
        val ADD_TODO_CHANGE_CODE = 2
        val NOTIF_ID = "edu.towson.cosc431"
        var THREAD_ID = 0
    }
}

