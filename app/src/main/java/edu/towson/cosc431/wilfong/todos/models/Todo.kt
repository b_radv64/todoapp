package edu.towson.cosc431.wilfong.todos.models

import com.google.gson.annotations.SerializedName

data class TODOList (
    @SerializedName("id")
    val id: Int,
    @SerializedName("title")
    val title: String,
    @SerializedName("contents")
    val contents: String,
    @SerializedName("completed")
    val isCompleted: Boolean,
    @SerializedName("image_url")
    val DateCreated: String
)
data class TODOListImage(
    @SerializedName("id")
    val id: Int,
    @SerializedName("title")
    val title: String,
    @SerializedName("contents")
    val contents: String,
    @SerializedName("completed")
    val isCompleted: Boolean,
    @SerializedName("image_url")
    val DateCreated: String,
    val image: String
)