package edu.towson.cosc431.wilfong.todos


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.button_fragment.*
import kotlinx.android.synthetic.main.button_fragment.view.*
import kotlinx.android.synthetic.main.button_fragment.view.btn_All

class SortingFragment : Fragment(), View.OnClickListener{

    interface OnTaskChange {
        enum class Rating { All, Active, Completed}
        fun onTaskChange(rating: Rating)
    }

    private var currentRating: OnTaskChange.Rating = OnTaskChange.Rating.All

    private var listener: OnTaskChange? = null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =  inflater.inflate(R.layout.button_fragment, container, false)

        view.btn_All.setOnClickListener (this)
        view.btn_Active.setOnClickListener (this)
        view.btn_completed.setOnClickListener (this)
        return view
    }
    override fun onClick(view: View?){
        //resetView()
        currentRating = when(view?.id){
            R.id.btn_All -> {
                btn_All.setBackgroundColor(resources.getColor(R.color.red))
                btn_Active.setBackgroundColor(resources.getColor(R.color.colorPrimary))
                btn_completed.setBackgroundColor(resources.getColor(R.color.colorPrimary))
                OnTaskChange.Rating.All
            }
            R.id.btn_Active -> {
                btn_Active.setBackgroundColor(resources.getColor(R.color.red))
                btn_All.setBackgroundColor(resources.getColor(R.color.colorPrimary))
                btn_completed.setBackgroundColor(resources.getColor(R.color.colorPrimary))
                OnTaskChange.Rating.Active
            }
            R.id.btn_completed -> {
                btn_completed.setBackgroundColor(resources.getColor(R.color.red))
                btn_Active.setBackgroundColor(resources.getColor(R.color.colorPrimary))
                btn_All.setBackgroundColor(resources.getColor(R.color.colorPrimary))
                OnTaskChange.Rating.Completed
            }
            else -> throw Exception("Unexpected view in onClick")
        }
        listener?.onTaskChange(currentRating)
    }

    fun setOnTaskChange(listener: OnTaskChange) {
        this.listener = listener
    }

}

