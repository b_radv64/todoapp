package edu.towson.cosc431.wilfong.todos


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import edu.towson.cosc431.wilfong.todos.interfaces.TODOController
import edu.towson.cosc431.wilfong.todos.models.TODOList
import kotlinx.android.synthetic.main.button_fragment.view.*
import kotlinx.android.synthetic.main.todo_view.view.*

class TODOAdapter(private val controller: TODOController) : RecyclerView.Adapter<TodoHolder>()  {
    override fun getItemCount(): Int {
        return controller.taskCache.getCount()
    }

    override fun onBindViewHolder(holder: TodoHolder, position: Int) {
        val list = controller.taskCache.getTask(position)
        holder.bindTask(list)
        controller.getIcon(list.DateCreated) { icon ->
            if(holder.adapterPosition == position){
                holder.itemView.loader.visibility = View.INVISIBLE
                holder.itemView.icon.setImageBitmap(icon)
                holder.itemView.icon.visibility = View.VISIBLE
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoHolder{
        val view = LayoutInflater.from(parent.context).inflate(R.layout.todo_view, parent, false)
        val viewHolder = TodoHolder(view)


         view.constraintLay.setOnLongClickListener  {
             val position = viewHolder.adapterPosition
             controller.deleteTask(position)
             this.notifyItemRemoved(position)
             true

        }

        view.completeCheck.setOnClickListener {
            val position = viewHolder.adapterPosition
            controller.completeCheck(position)
            this.notifyItemChanged(position)
        }

        view.constraintLay.setOnClickListener {
            val position = viewHolder.adapterPosition
            controller.editTask(position)
            this.notifyItemChanged(position)

        }


        return viewHolder
    }

}
class TodoHolder(view: View) : RecyclerView.ViewHolder(view){
    fun bindTask(List: TODOList){
        itemView.Todo_Title.text= List.title
        itemView.Todo_Contents.text= List.contents
        itemView.Todo_Date.text= List.DateCreated
        itemView.completeCheck.isChecked= List.isCompleted
    }
}

