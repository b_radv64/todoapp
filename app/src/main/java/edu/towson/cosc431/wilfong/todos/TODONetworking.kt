package edu.towson.cosc431.wilfong.todos

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Handler
import android.util.Log
import android.widget.Toast
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.BitmapRequestListener
import com.androidnetworking.interfaces.ParsedRequestListener
import com.google.gson.Gson
import edu.towson.cosc431.wilfong.todos.models.TODOList
import java.io.*
import java.net.URL
import java.util.concurrent.ThreadPoolExecutor
import javax.net.ssl.HttpsURLConnection

class TODONetworking{
    companion object{
        val API_URL = "https://my-json-server.typicode.com/rvalis-towson/todos_api/todos"

    }
    lateinit var executor: ThreadPoolExecutor
    lateinit var handler: Handler
    fun fetchTask(callback: (List<TODOList>) -> Unit){
        AndroidNetworking.get(API_URL)
            .setTag(this)
            .setPriority(Priority.LOW)
            .build()
            .getAsObjectList(TODOList::class.java, object: ParsedRequestListener<List<TODOList>> {
                override fun onResponse(response: List<TODOList>?) {
                    if(response != null){
                        callback(response)
                    }else{
                        throw Exception("Error fetching people")
                    }
                }

                override fun onError(anError: ANError?) {
                    Log.d("Check1", anError.toString())
                }

            })

    }

    fun fetchIcon(iconUrl: String, callback: (Bitmap) -> Unit) {
        AndroidNetworking.get(iconUrl)
            .setTag("imageRequestTag")
            .setPriority(Priority.LOW)
            .setBitmapMaxHeight(400)
            .setBitmapMaxWidth(400)
            .setBitmapConfig(Bitmap.Config.ARGB_8888)
            .build()
            .getAsBitmap(object: BitmapRequestListener {
                override fun onResponse(response: Bitmap?) {
                    if(response != null){
                        callback(response)
                    }else{
                        throw java.lang.Exception("Errors")
                    }
                }

                override fun onError(anError: ANError?) {
                    Log.d("Check1", anError.toString())
                }

            })
    }}

   /* fun fetchTask(url: String, callback: (Bitmap) -> Unit) {
        val cachedBitmap = checkCache(url)
        if(cachedBitmap == null) {
            executor.execute {
                val url_url = URL("${url}?delay=3")
                val connection = url_url.openConnection() as HttpsURLConnection?
                if (connection == null) {
                    //callback(null)
                } else {
                    //Thread.sleep(3000)
                    connection.doInput = true
                    connection.requestMethod = "GET"
                    connection.connect()
                    val bitmap = BitmapFactory.decodeStream(connection.inputStream)
                    cacheFile(url, bitmap)
                    // make sure the callback runs on the UI thread
                    handler.post {
                        callback(bitmap)
                    }
                }
            }
        } else {
            handler.post {
                callback(cachedBitmap)
            }
        }
    }

    private fun cacheFile(url: String, bitmap: Bitmap?) {
        if(bitmap != null) {
            getLast2PathSegmentsAsString(url)?.let { filename ->
                val cacheFile = File(cacheDir, filename)
                val fos = FileOutputStream(cacheFile)
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos)
                fos.close()
            }
        }
    }

    private fun getLast2PathSegmentsAsString(url: String) : String? {
        val segments = Uri.parse(url)?.pathSegments
        val last2 = segments?.subList(segments.size - 2, segments.size)
        return last2?.reduce { acc, s -> acc + s }
    }

    private fun checkCache(url: String): Bitmap? {
        getLast2PathSegmentsAsString(url)?.let { filename ->
            val cacheFile = File(cacheDir, filename)
            return if(cacheFile.exists()) {
                BitmapFactory.decodeStream(cacheFile.inputStream())
            } else {
                null
            }
        }
        return null
    }


    /**
     * Converts the contents of an InputStream to a String.
     */
    @Throws(IOException::class, UnsupportedEncodingException::class)
    fun readStream(stream: InputStream, maxReadSize: Int): String? {
        val reader: Reader? = InputStreamReader(stream, "UTF-8")
        val rawBuffer = CharArray(maxReadSize)
        val buffer = StringBuffer()
        var readSize: Int = reader?.read(rawBuffer) ?: -1
        var maxReadBytes = maxReadSize
        while (readSize != -1 && maxReadBytes > 0) {
            if (readSize > maxReadBytes) {
                readSize = maxReadBytes
            }
            buffer.append(rawBuffer, 0, readSize)
            maxReadBytes -= readSize
            readSize = reader?.read(rawBuffer) ?: -1
        }
        stream.close()
        return buffer.toString()
    }
}*/