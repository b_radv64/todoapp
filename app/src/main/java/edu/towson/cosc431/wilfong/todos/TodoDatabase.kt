package edu.towson.cosc431.wilfong.todos

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns
import edu.towson.cosc431.wilfong.todos.models.TODOList
import kotlinx.android.synthetic.main.activity_main.*

object TaskContract {
    object TaskEntry : BaseColumns {
        const val TABLE_NAME = "Todo"
        const val COLUMN_NAME_TITLE = "title"
        const val COLUMN_NAME_CONTENTS = "contents"
        const val COLUMN_NAME_ISCOMPLETED = "isCompleted"
        const val COLUMN_NAME_DATECREATED = "DateCreated"
        const val COLUMN_NAME_DELETED = "deleted"


    }
}

interface IDatabase {
    fun addTodo(task: TODOList)
    fun getTodos(): List<TODOList>
    fun getTodo(id: Int) : TODOList?
    fun getCompleted(): List<TODOList>
    fun getActive(): List<TODOList>
    fun deleteTodo(task: TODOList)
    fun updateTodo(task: TODOList)
}


// CREATE TABLE songs (_id INTEGER PRIMARY KEY AUTOINCREMENT, song_name TEXT, song_artist INTEGER, song_awesome BOOL, deleted BOOL DEFAULT 0)
private const val CREATE_SONG_TABLE = "CREATE TABLE ${TaskContract.TaskEntry.TABLE_NAME} (" +
        "${BaseColumns._ID} INTEGER PRIMARY KEY AUTOINCREMENT, " +
        "${TaskContract.TaskEntry.COLUMN_NAME_TITLE} TEXT, " +
        "${TaskContract.TaskEntry.COLUMN_NAME_CONTENTS} TEXT, " +
        "${TaskContract.TaskEntry.COLUMN_NAME_ISCOMPLETED} BOOL,  " +
        "${TaskContract.TaskEntry.COLUMN_NAME_DATECREATED} TEXT, "  +
        "${TaskContract.TaskEntry.COLUMN_NAME_DELETED} BOOL DEFAULT 0" +
        ")"

private const val DELETE_TODO_TABLE = "DROP TABLE IF EXISTS ${TaskContract.TaskEntry.TABLE_NAME}"

class TodoDatabase(ctx: Context) : IDatabase {

    class SongDbHelper(ctx: Context) : SQLiteOpenHelper(ctx, DATABASE_NAME, null, DATABASE_VERSION) {
        override fun onCreate(db: SQLiteDatabase?) {
            db?.execSQL(CREATE_SONG_TABLE)
        }

        override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
            db?.execSQL(DELETE_TODO_TABLE)
            onCreate(db)
        }

        companion object{
            val DATABASE_NAME = "todo.db"
            val DATABASE_VERSION = 1
        }
    }

    private val db : SQLiteDatabase

    init {
        db = SongDbHelper(ctx).writableDatabase
    }

    override fun addTodo(task: TODOList) {
        val cvs = toContentValues(task)
        db.insert(TaskContract.TaskEntry.TABLE_NAME, null, cvs)

    }

    override fun getTodos(): List<TODOList> {
        val projection = arrayOf(BaseColumns._ID, TaskContract.TaskEntry.COLUMN_NAME_TITLE, TaskContract.TaskEntry.COLUMN_NAME_CONTENTS, TaskContract.TaskEntry.COLUMN_NAME_ISCOMPLETED, TaskContract.TaskEntry.COLUMN_NAME_DATECREATED) //which columns do we want?
        val sortOrder = "${BaseColumns._ID} ASC" // how should the songs be ordered
        val selection = "${TaskContract.TaskEntry.COLUMN_NAME_DELETED} = ?" // where clause?
        val selectionArgs = arrayOf("0")

        val cursor = db.query(
            TaskContract.TaskEntry.TABLE_NAME,
            projection,
            selection,
            selectionArgs,
            null,
            null,
            sortOrder
        )
        val tasks = mutableListOf<TODOList>()
        with(cursor){
            while(cursor.moveToNext()){
                val id = getInt(getColumnIndex(BaseColumns._ID))
                val title = getString(getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_TITLE))
                val contents = getString(getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_CONTENTS))
                val dateCreated = getString(getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_DATECREATED))
                val isCompleted = getInt(getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_ISCOMPLETED)) > 0
                val task = TODOList(id, title, contents, isCompleted, dateCreated)
                tasks.add(task)
            }
        }
        return tasks
    }

    override fun getTodo(id: Int): TODOList? {
        val projection = arrayOf(BaseColumns._ID, TaskContract.TaskEntry.COLUMN_NAME_TITLE, TaskContract.TaskEntry.COLUMN_NAME_CONTENTS, TaskContract.TaskEntry.COLUMN_NAME_ISCOMPLETED, TaskContract.TaskEntry.COLUMN_NAME_DATECREATED) //which columns do we want?
        val sortOrder = "${BaseColumns._ID} ASC" // how should the songs be ordered
        val selection = "${TaskContract.TaskEntry.COLUMN_NAME_DELETED} = ? AND ${BaseColumns._ID} = ?"  // where deleted = ? AND _ID = ?
        val selectionArgs = arrayOf("0", id.toString()) //

        val cursor = db.query(
            TaskContract.TaskEntry.TABLE_NAME,
            projection,
            selection,
            selectionArgs,
            null,
            null,
            sortOrder
        )
        val tasks = mutableListOf<TODOList>()
        with(cursor){
            while(cursor.moveToNext()){
                val id = getInt(getColumnIndex(BaseColumns._ID))
                val title = getString(getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_TITLE))
                val contents = getString(getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_CONTENTS))
                val dateCreated = getString(getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_DATECREATED))
                val isCompleted = getInt(getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_ISCOMPLETED)) > 0
                val task = TODOList(id, title, contents, isCompleted, dateCreated)
                tasks.add(task)
            }
        }
        if(tasks.size == 1) return tasks[0]
        return null
    }

    override fun getCompleted(): List<TODOList> {
        val cursor = db.rawQuery("SELECT * FROM ${TaskContract.TaskEntry.TABLE_NAME} WHERE ${TaskContract.TaskEntry.COLUMN_NAME_ISCOMPLETED} = 1", null)
        val tasks = mutableListOf<TODOList>()
        with(cursor){
            while(cursor.moveToNext()){
                val id = getInt(getColumnIndex(BaseColumns._ID))
                val title = getString(getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_TITLE))
                val contents = getString(getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_CONTENTS))
                val dateCreated = getString(getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_DATECREATED))
                val isCompleted = getInt(getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_ISCOMPLETED)) > 0
                val task = TODOList(id, title, contents, isCompleted, dateCreated)
                tasks.add(task)
            }
        }
        return tasks
    }
    override fun getActive(): List<TODOList> {
      val cursor = db.rawQuery("SELECT * FROM ${TaskContract.TaskEntry.TABLE_NAME} WHERE ${TaskContract.TaskEntry.COLUMN_NAME_ISCOMPLETED} = 0", null)
        val tasks = mutableListOf<TODOList>()
        with(cursor){
            while(cursor.moveToNext()){
                val id = getInt(getColumnIndex(BaseColumns._ID))
                val title = getString(getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_TITLE))
                val contents = getString(getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_CONTENTS))
                val dateCreated = getString(getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_DATECREATED))
                val isCompleted = getInt(getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_ISCOMPLETED)) > 0
                val task = TODOList(id, title, contents, isCompleted, dateCreated)
                tasks.add(task)
            }
        }
        return tasks
    }
    override fun deleteTodo(task: TODOList) {
        val cvs = toContentValues(task)
        cvs.put(TaskContract.TaskEntry.COLUMN_NAME_DELETED, "1")
        val selection = "${BaseColumns._ID} = ?"
        val selectionArgs = arrayOf(task.id.toString())
        db.update(TaskContract.TaskEntry.TABLE_NAME, cvs, selection, selectionArgs)
    }

    override fun updateTodo(task: TODOList) {
        val cvs = toContentValues(task)
        val selection = "${BaseColumns._ID} = ?"
        val selectionArgs = arrayOf(task.id.toString())
        db.update(TaskContract.TaskEntry.TABLE_NAME, cvs, selection, selectionArgs)
    }

    private fun toContentValues(task: TODOList): ContentValues {
        val cv = ContentValues()
        cv.put(TaskContract.TaskEntry.COLUMN_NAME_TITLE, task.title)
        cv.put(TaskContract.TaskEntry.COLUMN_NAME_CONTENTS, task.contents)
        cv.put(TaskContract.TaskEntry.COLUMN_NAME_ISCOMPLETED, task.isCompleted)
        cv.put(TaskContract.TaskEntry.COLUMN_NAME_DATECREATED, task.DateCreated)
        return cv
    }
}