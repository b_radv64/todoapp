package edu.towson.cosc431.wilfong.todos.interfaces

import edu.towson.cosc431.wilfong.todos.models.TODOList

interface TODORepository {
    fun getCount(): Int
    fun getTask(idx: Int): TODOList?
    fun getAll(): List<TODOList>
    fun remove(task: TODOList)
    fun updateTask(task: TODOList)
    fun addTask(task: TODOList)
    fun completed(): List<TODOList>
    fun active(): List<TODOList>
    fun addAllTaskdb(task: List<TODOList>)
}

interface TODOCache {
    fun getCount(): Int
    fun getFilterCount(): Int
    fun getTask(idx: Int): TODOList
    fun getFilterTask(idx: Int): TODOList
    fun getAll(): List<TODOList>
    fun remove(task: TODOList)
    fun updateTask(idx: Int, task: TODOList)
    fun replace(idx: Int, task: TODOList)
    fun addTask(task: TODOList)
    fun addTaskFilter(task: TODOList)
    fun removeEverything()
    fun filterRemoveEverything()
    fun refresh(songs: List<TODOList>)
    fun addAllTask(task: List<TODOList>)
}